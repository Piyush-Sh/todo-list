import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter,Route, Switch } from 'react-router-dom';
import App from "./App";
import User from "./Login";

const Index=()=>{
    return(
    <div>
    <BrowserRouter>
    <Switch>
        <Route exact path="/" component={User} />
        <Route path="/status" component={App} />
    </Switch>
    </BrowserRouter>
    </div>
    )
}
ReactDOM.render(<Index/>,document.getElementById("root"));