import React from "react";

const inputChild=(props: { input1: string | number | readonly string[] | undefined; input2: React.ChangeEventHandler<HTMLInputElement> | undefined; })=>{
    return(
        <div>
        <label htmlFor="input" className="itemlabel">Enter Item  </label>
        <input className="item" value={props.input1} id="input" onChange={props.input2} autoFocus/>
        </div>
    )
}
export default inputChild