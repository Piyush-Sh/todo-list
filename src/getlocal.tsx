import React from "react";

const useLocalStorage=()=>{
    function getStorage():[]{
        const listget =localStorage.getItem('data');
        if(listget){
            return JSON.parse(String(localStorage.getItem('data')));
        }else{
            return [];
        }
    }

    return getStorage;
}
export default useLocalStorage;