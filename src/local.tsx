import { useEffect } from 'react';

const useStorage=(items:object[])=>{

    useEffect(()=>{
        localStorage.setItem("data",JSON.stringify(items));
      },[items]);
}

export default useStorage;