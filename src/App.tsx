import React, { useState } from 'react';
import "./app.css";
import Input from './Input';
import Button from './button';
import { RiDeleteBin6Line,RiEdit2Fill } from "react-icons/ri";
import useStorage from "./local";
import useLocalStorage from "./getlocal";
import { Link, NavLink } from 'react-router-dom';

const App=()=>{
    const [newInputData,setNewInputData]=useState<string>("");
    const [list, setList] = useState<any[]>(useLocalStorage());
    const [edititem,updateedititem]=useState(true);
    const [editinput,seteditinput]=useState<number|string>("");

    
    function getValue(event: { target: { value: React.SetStateAction<string>; }; }){   
        setNewInputData(event.target.value);
    };
    const updateData=()=>{
        if(!newInputData){
        alert('Please Fill The Data');
        }
        else if(newInputData && !edititem){
            setList(list.map((ele:any)=>{
                if(ele.id===editinput){
                    return{...ele,name:newInputData}
                }
                return ele;
            }))
            
        updateedititem(true);
        setNewInputData('');
        seteditinput("");
        }
        else
        {
        const allinput:object={id: new Date().getTime().toString(),name:newInputData};
        setList([...list,allinput]);
        setNewInputData("");  
        }
        };
    const deleteEle=(index:any)=>{
        const updatelist=list.filter((items )=>{
            return index !== items.id;
        });
        setList(updatelist) 
    }
    const editEle=(id:number|string)=>{
        let edit=list.find((ele)=>{
             return ele.id===id
        });
        updateedititem(false);
        setNewInputData(edit.name);
        seteditinput(id);
    }   
    const cutit=(id:number)=>{
        const newlist=list.map((ele)=>{
            if(ele.id===id){
                return {...ele,done:!ele.done}
            }
        return ele;
        })
        setList(newlist);
    }
    useStorage(list);
    return (
    <div className="Divbody">
        <div>
        <button className="Logoutbutton"><NavLink to="/" style={{ textDecoration: 'none' }} className="Logoutbuttonlink">Logout</NavLink></button>
        </div>
        <Input data={newInputData} value={getValue}/>
        <div>
            { list.map((listitems)=>{
                return (<>
                <div key={listitems.id} className="Showmain">
                    <div className="Show1">
                        <input type="checkbox" onClick={()=>cutit(listitems.id)}></input>
                    </div>
                    <div className="Show2">
                        <h3 style={{textDecoration: listitems.done ? "line-through" : "none"}}>{listitems.name}</h3>
                    </div>
                    <div className="Show3">
                        <span title="Delete Item" onClick={()=>deleteEle(listitems.id)}><RiDeleteBin6Line/></span>
                    </div>
                    <div className="Show4">
                        <span title="Edit Item" onClick={()=>editEle(listitems.id)}><RiEdit2Fill/></span>
                    </div>
                </div>
                </>)
            
            })}
        </div>
        {
            edititem ? <Button data={updateData}/>:<button className="btn" onClick={updateData}>Edit</button> 
        }
    </div>
    )
    }

export default App;