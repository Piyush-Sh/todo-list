import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Status from "./App";
var button = false;
const User=()=>{

    const friendList = [
        { id: 1, name: 'piyush',Email:'piyush@gmail.com',Password:'1234',isonline:false},
        { id: 2, name: 'Ravi',Email:'ravi@gmail.com',Password:'1234',isonline:false },
        { id: 3, name: 'omesh',Email:'omesh@gmail.com',Password:'1234',isonline:false },
      ];
      useEffect(() => {
          button=false;
      });
      
    const [email,updateemail]=useState<string>("");
    const [password,updatepassword]=useState<string>("");
    const [error,seterror] =useState<String|null>(null);
    

    function getValue1(event:any){   
        updateemail(event.target.value);
        seterror(null);

    }
      function getValue2(event:any){   
        updatepassword(event.target.value);
        seterror(null);
    }
    
    function handle(e: any){
        e.preventDefault();
        console.log(button);
        friendList.map(friend=>
            {
            if(friend.Email === email && friend.Password === password) 
            {
                console.log(email);
                friend.isonline=true;
                <Status/>
                localStorage.setItem(String(friend.id),String(friend.isonline));
                //console.log(localStorage.getItem(String(friend.id)));
                //console.log(friend.isonline);
                button=true;
            }
            }
            )
            console.log(button);
            button ? seterror("User Id and Password Verified") : seterror("Wrong User Id and Password");
        };
    return(
        
        <div>
            <div className="Logindiv">
            <form onSubmit={handle}>
            <div><h2>Login Form</h2></div>
        <div>
            <label htmlFor="email" id="email" className="Loginlabel">Email</label>
            <input className="Logininput1" type="e-mail" name="email" value={email} onChange={getValue1} required/>
        </div>
            <br/>
        <div><label htmlFor="password" id="password" className="Loginlabel">Password</label>
            <input className="Logininput2" type="password" name="password" value={password} onChange={getValue2} required/>
        </div>    
            <br/>
        <div>    
            {console.log(button)}
           {button ? <button type="submit" className="Loginbutton"><Link to="/status" style={{ textDecoration: 'none' }} className="Loginbuttonlink">Login</Link></button> :<button className="Loginbutton" type="submit">Login</button> }
        </div>
           <div className="Errorkdiv"><h2 className="Errorheading">{error}</h2></div>
           </form>
        </div>
        </div>

    )


}
export default User;